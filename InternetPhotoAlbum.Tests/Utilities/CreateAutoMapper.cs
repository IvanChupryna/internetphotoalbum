﻿using AutoMapper;
using InternetPhotoAlbum.BLL.AutoMapper;
using InternetPhotoAlbum.WEB.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.Tests.Utilities
{
    internal static class CreateAutoMapper
    {
        public static Mapper GetBLLMapper()
        {
            Profile mapperBllProfile = new MapperBLLProfile();
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.AddProfile(mapperBllProfile));
            return new Mapper(config);

        }

        public static Mapper GetPLMapper()
        {
            Profile mapperWebProfile = new MapperWebProfile();
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.AddProfile(mapperWebProfile));
            return new Mapper(config);
        }
    }
}
