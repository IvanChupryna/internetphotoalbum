﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.Tests.Utilities
{
    internal static class Comparer
    {
        public static bool ComparePosts(PostDTO firstPost, PostDTO secondPost)
        {
            return firstPost.Id == secondPost.Id
                && firstPost.Image == secondPost.Image
                && firstPost.PostedDate == secondPost.PostedDate
                && firstPost.UserId == secondPost.UserId;
        }

        public static bool CompareLikes(LikeDTO firstLike, LikeDTO secondLike)
        {
            return firstLike.PostId == secondLike.PostId
                && firstLike.UserId == secondLike.UserId;
        }

        public static bool CompareProfiles(UserProfileDTO firstProfile, UserProfileDTO secondProfile)
        {
            return firstProfile.Id == secondProfile.Id
                && firstProfile.Username == secondProfile.Username
                && firstProfile.Email == secondProfile.Email
                && firstProfile.PhotoAlbumUserId == secondProfile.PhotoAlbumUserId;
        }

    }
}
