﻿using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services;
using InternetPhotoAlbum.BLL.Validation;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using InternetPhotoAlbum.Tests.Utilities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace InternetPhotoAlbum.Tests.BusinessLogicLayer_Tests
{
    public class PostServiceTests
    {
        [Fact]
        public async Task PostService_AddAsync_AddsPostToDbContext()
        {
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.AddAsync(It.IsAny<Post>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var postToAdd = new PostDTO()
            {
                Id = 10,
                Image = "Image10",
                PostedDate = new DateTime(2021, 9, 1),
                UserId = "2"
            };
            await postService.AddAsync(postToAdd);

            mockRepo.Verify(x => x.PostRepository.AddAsync(
                It.Is<Post>(p => p.Id == postToAdd.Id
                && p.Image == postToAdd.Image
                && p.PostedDate == postToAdd.PostedDate
                && p.UserId == postToAdd.UserId)), Times.Once);
            mockRepo.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task PostService_AddAsync_ThrowPostExceptionWithEmptyImage()
        {
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.AddAsync(It.IsAny<Post>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var postToAdd = new PostDTO()
            {
                Id = 10,
                PostedDate = new DateTime(2021, 9, 1),
                UserId = "2"
            };

            await Assert.ThrowsAsync<PostException>(() => postService.AddAsync(postToAdd));
        }

        [Fact]
        public void PostService_GetAll_ReturnsIEnumerableOfPostDTOs()
        {
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAll())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);
            var expected = PostDTOs.ToList();

            var actual = postService.GetAll().ToList();

            Assert.IsAssignableFrom<IEnumerable<PostDTO>>(actual);
            Assert.Equal(expected.Count, actual.Count);
            for(int i = 0; i < expected.Count; i++)
            {
                Assert.True(Comparer.ComparePosts(expected[i], actual[i]));
            }
        }

        [Fact]
        public async Task PostService_GetByIdAsync_ReturnsPostDTO()
        {
            var expected = PostDTOs.First();
            var idToGet = 1;

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetByIdAsync(It.IsAny<int>()))
            .ReturnsAsync(Posts.First());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var actual = await postService.GetByIdAsync(idToGet);

            Assert.IsType<PostDTO>(actual);
            Assert.True(Comparer.ComparePosts(expected, actual));
        }

        [Fact]
        public void PostService_GetPostsByUserName_ReturnsIEnumerableOfPostDTOs()
        {
            var expected = PostDTOs.Where(p => p.UserId == "1").ToList();
            var usernameToGet = "John";

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAll())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var actual = postService.GetPostsByUsername(usernameToGet).ToList();

            Assert.IsAssignableFrom<IEnumerable<PostDTO>>(actual);
            Assert.Equal(expected.Count, actual.Count);
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.True(Comparer.ComparePosts(expected[i], actual[i]));
            }
        }

        [Fact]
        public void PostService_GetPostsByUserName_ThrowsPostException()
        {
            var usernameToGet = "Ivan";

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAll())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            Assert.Throws<PostException>(() => postService.GetPostsByUsername(usernameToGet));
        }

        [Fact]
        public void PostService_GetByDate_ReturnsIEnumerableOfPostDTOs()
        {
            var dateToGet = new DateTime(2021, 9, 7);
            var expected = PostDTOs.Where(p => p.PostedDate == dateToGet).ToList();

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAll())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var actual = postService.GetByDate(dateToGet).ToList();

            Assert.IsAssignableFrom<IEnumerable<PostDTO>>(actual);
            Assert.Equal(expected.Count, actual.Count);
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.True(Comparer.ComparePosts(expected[i], actual[i]));
            }
        }

        [Fact]
        public void PostService_GetByDate_ThrowsPostException()
        {
            var dateToGet = DateTime.Now.AddDays(1);

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAll())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            Assert.Throws<PostException>(() => postService.GetByDate(dateToGet));
        }

        [Fact]
        public void PostService_GetSortedByDate_ReturnsSortedByDateIEnumerableOfPostDTOs()
        {
            var expected = PostDTOs.OrderByDescending(p => p.PostedDate).ToList();

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAll())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var actual = postService.GetSortedByDate().ToList();

            Assert.IsAssignableFrom<IEnumerable<PostDTO>>(actual);
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.True(Comparer.ComparePosts(expected[i], actual[i]));
            }
        }

        [Fact]
        public void PostService_GetSortedByLikes_ReturnsSortedByLikesIEnumerableOfPostDTOs()
        {
            var expected = PostDTOs.OrderByDescending(p => p.LikesIds.Count).ToList();

            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.GetAllWithRelated())
            .Returns(Posts.AsQueryable());
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var actual = postService.GetSortedByLikes().ToList();

            Assert.IsAssignableFrom<IEnumerable<PostDTO>>(actual);
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.True(Comparer.ComparePosts(expected[i], actual[i]));
            }
        }

        [Fact]
        public async Task PostService_UpdateAsync_UpdatesPostInDbContext()
        {
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.Update(It.IsAny<Post>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var postToUpdate = new PostDTO()
            {
                Id = 3,
                Image = "UpdateImage",
                PostedDate = new DateTime(2021, 9, 7),
                UserId = "2",
                PostText = "Update"
            };
            await postService.UpdateAsync(postToUpdate);

            mockRepo.Verify(x => x.PostRepository.Update(
                It.Is<Post>(p => p.Id == postToUpdate.Id
                && p.Image == postToUpdate.Image
                && p.PostText == postToUpdate.PostText)), Times.Once);
            mockRepo.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task PostService_UpdateAsync_ThrowPostExceptionWithEmptyImage()
        {
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.Update(It.IsAny<Post>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            var postToUpdate = new PostDTO()
            {
                Id = 1,
                PostedDate = new DateTime(2021, 9, 1),
                UserId = "2"
            };

            await Assert.ThrowsAsync<PostException>(() => postService.UpdateAsync(postToUpdate));
        }

        [Fact]
        public async Task PostService_RemoveAsync_RemovesPostFromDbContext()
        {
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.PostRepository.RemoveByIdAsync(It.IsAny<int>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var postService = new PostService(mockRepo.Object, mapper);

            int idToRemove = 1;
            await postService.RemoveByIdAsync(idToRemove);

            mockRepo.Verify(x => x.PostRepository.RemoveByIdAsync(idToRemove), Times.Once);
            mockRepo.Verify(x => x.SaveAsync(), Times.Once);
        }

        private IEnumerable<PhotoAlbumUser> Users
        {
            get
            {
                return new List<PhotoAlbumUser>()
                {
                    new PhotoAlbumUser(){ Id = "1", UserName = "John", Email = "john@gmail.com"},
                    new PhotoAlbumUser(){ Id = "2", UserName = "Alex", Email = "alex@gmail.com"}
                };
            }
        }

        private IEnumerable<Post> Posts
        {
            get
            {
                return new List<Post>()
                {
                    new Post(){ Id = 1, UserId = "1", PostedDate = new DateTime(2021, 9, 5), Image = "Image1", User = Users.FirstOrDefault(u => u.Id == "1"), Likes = Likes.Where(l => l.PostId == 1).ToList() },
                    new Post(){ Id = 2, UserId = "1", PostedDate = new DateTime(2021, 9, 7), Image = "Image2", User = Users.FirstOrDefault(u => u.Id == "1"), Likes = Likes.Where(l => l.PostId == 2).ToList()},
                    new Post(){ Id = 3, UserId = "2", PostedDate = new DateTime(2021, 9, 7), Image = "Image3", User = Users.FirstOrDefault(u => u.Id == "2"), Likes = Likes.Where(l => l.PostId == 3).ToList()},
                    new Post(){ Id = 4, UserId = "2", PostedDate = new DateTime(2021, 9, 8), Image = "Image4", User = Users.FirstOrDefault(u => u.Id == "2"), Likes = Likes.Where(l => l.PostId == 4).ToList()}
                };
            }
        }

        private IEnumerable<Like> Likes
        {
            get
            {
                return new List<Like>()
                {
                    new Like(){ Id = 1, PostId = 1, UserId = "1"},
                    new Like(){ Id = 2, PostId = 2, UserId = "1"},
                    new Like(){ Id = 3, PostId = 3, UserId = "2"},
                    new Like(){ Id = 4, PostId = 4, UserId = "2"},
                    new Like(){ Id = 5, PostId = 4, UserId = "2"},
                    new Like(){ Id = 6, PostId = 4, UserId = "1"},
                    new Like(){ Id = 7, PostId = 3, UserId = "1"},
                    new Like(){ Id = 8, PostId = 2, UserId = "1"}
                };
            }
        }

        private IEnumerable<PostDTO> PostDTOs
        {
            get
            {
                return new List<PostDTO>()
                {
                    new PostDTO(){ Id = 1, UserId = "1", PostedDate = new DateTime(2021, 9, 5), Image = "Image1", LikesIds = new List<int>(){1} },
                    new PostDTO(){ Id = 2, UserId = "1", PostedDate = new DateTime(2021, 9, 7), Image = "Image2", LikesIds = new List<int>(){2, 8}},
                    new PostDTO(){ Id = 3, UserId = "2", PostedDate = new DateTime(2021, 9, 7), Image = "Image3", LikesIds = new List<int>(){3, 7}},
                    new PostDTO(){ Id = 4, UserId = "2", PostedDate = new DateTime(2021, 9, 8), Image = "Image4", LikesIds = new List<int>(){4, 5, 6}}
                };

            }
        }
    }
}
