﻿using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services;
using InternetPhotoAlbum.BLL.Validation;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using InternetPhotoAlbum.Tests.Utilities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace InternetPhotoAlbum.Tests.BusinessLogicLayer_Tests
{
    public class LikeServiceTests
    {

        [Fact]
        public async Task LikeService_GetAllLikesOfCurrentPost_ReturnIEnumerableOfLikeDTOs()
        {
            var postId = 1;
            var expected = LikeDTOs.ToList();
            var postRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            postRepo.Setup(x => x.PostRepository.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(Posts.FirstOrDefault(p => p.Id == postId)));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var likeService = new LikeService(postRepo.Object, mapper);

            var actual = (await likeService.GetAllLikesOfCurrentPostAsync(postId)).ToList();

            Assert.IsAssignableFrom<IEnumerable<LikeDTO>>(actual);
            for(int i = 0; i < expected.Count; i++)
            {
                Assert.True(Comparer.CompareLikes(expected[i], actual[i]));
            }
        }

        [Fact]
        public async Task LikeService_GetAllLikesOfCurrentPost_ThrowsPostExceptionWithNotExistingPost()
        {
            var postId = 3;
            var postRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            postRepo.Setup(x => x.PostRepository.GetByIdAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(Posts.FirstOrDefault(p => p.Id == postId)));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var likeService = new LikeService(postRepo.Object, mapper);

            await Assert.ThrowsAsync<PostException>(() => likeService.GetAllLikesOfCurrentPostAsync(postId));
        }

        [Fact]
        public async Task LikeService_AddLikeToPostAsync_AddsLikeToPost()
        {
            var postId = 2;
            var userId = "1";
            var likeRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            likeRepo.Setup(x => x.LikeRepository.AddAsync(It.IsAny<Like>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var likeService = new LikeService(likeRepo.Object, mapper);

            await likeService.AddLikeToPostAsync(userId, postId);

            likeRepo.Verify(x => x.LikeRepository.AddAsync(
                It.Is<Like>(l => l.PostId == postId
                && l.UserId == userId)), Times.Once);
            likeRepo.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task LikeService_AddLikeToPostAsync_ThrowsLikeExceptionWithExistingLike()
        {
            var postId = 1;
            var userId = "1";
            var likeRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            likeRepo.Setup(x => x.LikeRepository.GetAll())
                .Returns(Likes.AsQueryable);
            var mapper = CreateAutoMapper.GetBLLMapper();
            var likeService = new LikeService(likeRepo.Object, mapper);

            await Assert.ThrowsAsync<LikeException>(() => likeService.AddLikeToPostAsync(userId, postId));
        }

        [Fact]
        public async Task LikeService_RemoveLikeFromPostAsync_RemovesLikeFromPost()
        {
            var postId = 1;
            var userId = "1";
            var likeRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            likeRepo.Setup(x => x.LikeRepository.GetAll())
                .Returns(Likes.AsQueryable);
            likeRepo.Setup(x => x.LikeRepository.Remove(It.IsAny<Like>()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var likeService = new LikeService(likeRepo.Object, mapper);

            await likeService.RemoveLikeFromPostAsync(userId, postId);

            likeRepo.Verify(x => x.LikeRepository.Remove(
                It.Is<Like>(l => l.PostId == postId
                && l.UserId == userId)), Times.Once);
            likeRepo.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Fact]
        public async Task LikeService_RemoveLikeFromPostAsync_ThrowsLikeExceptionWithNotExistingLike()
        {
            var postId = 2;
            var userId = "1";
            var likeRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            likeRepo.Setup(x => x.LikeRepository.GetAll())
                .Returns(Likes.AsQueryable);
            var mapper = CreateAutoMapper.GetBLLMapper();
            var likeService = new LikeService(likeRepo.Object, mapper);

            await Assert.ThrowsAsync<LikeException>(() => likeService.RemoveLikeFromPostAsync(userId, postId));
        }

        private IEnumerable<PhotoAlbumUser> Users
        {
            get
            {
                return new List<PhotoAlbumUser>()
                {
                    new PhotoAlbumUser(){ Id = "1", UserName = "John", Email = "john@gmail.com"}
                };
            }
        }
        private IEnumerable<Post> Posts
        {
            get
            {
                return new List<Post>()
                {
                    new Post() { Id = 1, UserId = "1", PostedDate = new DateTime(2021, 9, 5), Image = "Image1", User = Users.FirstOrDefault(u => u.Id == "1"), Likes = Likes.Where(l => l.PostId == 1).ToList() },
                    new Post(){ Id = 2, UserId = "1", PostedDate = new DateTime(2021, 9, 7), Image = "Image2", User = Users.FirstOrDefault(u => u.Id == "1")}
                };
            }
        }
        private IEnumerable<Like> Likes
        {
            get
            {
                return new List<Like>()
                {
                    new Like(){ Id = 1, PostId = 1, UserId = "1"}
                };
            }
        }

        private IEnumerable<LikeDTO> LikeDTOs
        {
            get
            {
                return new List<LikeDTO>()
                {
                    new LikeDTO(){ PostId = 1, UserId = "1"}
                };
            }
        }
    }
}
