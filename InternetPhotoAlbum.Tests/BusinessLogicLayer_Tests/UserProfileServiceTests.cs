﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using System.Threading.Tasks;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using InternetPhotoAlbum.Tests.Utilities;
using InternetPhotoAlbum.BLL.Services;
using InternetPhotoAlbum.BLL.DTOs;
using System.Linq;

namespace InternetPhotoAlbum.Tests.BusinessLogicLayer_Tests
{
    public class UserProfileServiceTests
    {
        [Fact]
        public async Task UserProfileService_GetUserProfileByUserIdAsync_ReturnsUserProfileDTO()
        {
            var userId = "1";
            var expected = UserProfileDTOs.First();
            var mockRepo = new Mock<IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>>();
            mockRepo.Setup(x => x.UserProfileRepository.GetUserProfileByUserIdAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(UserProfiles.First()));
            var mapper = CreateAutoMapper.GetBLLMapper();
            var userProfileService = new UserProfileService(mockRepo.Object, mapper);

            var actual = await userProfileService.GetUserProfileByUserIdAsync(userId);

            Assert.True(Comparer.CompareProfiles(expected, actual));
        }

        private IEnumerable<PhotoAlbumUser> Users
        {
            get
            {
                return new List<PhotoAlbumUser>()
                { 
                    new PhotoAlbumUser(){Id = "1", UserName = "Kevin", Email = "kevin@gmail.com"},
                };
            }
        }

        private IEnumerable<UserProfile> UserProfiles
        {
            get
            {
                return new List<UserProfile>()
                {
                    new UserProfile(){Id = 1, Username = "Kevin", Email = "kevin@gmail.com", PhotoAlbumUserId = "1", PhotoAlbumUser = Users.First()}
                };
            }
        }

        private IEnumerable<UserProfileDTO> UserProfileDTOs
        {
            get
            {
                return new List<UserProfileDTO>()
                {
                    new UserProfileDTO(){Id = 1, Username = "Kevin", Email = "kevin@gmail.com", PhotoAlbumUserId = "1"}
                };
            }
        }
    }
}
