﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories;
using InternetPhotoAlbum.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace InternetPhotoAlbum.Tests.DataAccessLayer_Tests
{
    public class PostRepositoryTests
    {
        [Fact]
        public async Task PostRepository_AddAsync_AddsPostToDatabase()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);
                var postToAdd = new Post() { Id = 2 };

                await postRepo.AddAsync(postToAdd);
                await context.SaveChangesAsync();

                Assert.Equal(2, context.Posts.Count());
            }
        }

        [Fact]
        public async Task PostRepository_RemoveByIdAsync_RemovesPostFromDatabase()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);
                int idToDelete = 1;

                await postRepo.RemoveByIdAsync(idToDelete);
                await context.SaveChangesAsync();

                Assert.Equal(0, context.Posts.Count());
            }
        }

        [Fact]
        public async Task PostRepository_Update_UpdatesPostInDatabase()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);
                var editedPost = new Post()
                {
                    Id = 1,
                    UserId = "f2b1582b-e36e-44fe-9bae-42f9220d3499",
                    PostedDate = new DateTime(2021, 9, 9),
                    PostText = "Edit",
                    Image = "SomeImage"
                };

                postRepo.Update(editedPost);
                await context.SaveChangesAsync();

                Assert.Equal(1, editedPost.Id);
                Assert.Equal("f2b1582b-e36e-44fe-9bae-42f9220d3499", editedPost.UserId);
                Assert.Equal(new DateTime(2021, 9, 9), editedPost.PostedDate);
                Assert.Equal("Edit", editedPost.PostText);
                Assert.Equal("SomeImage", editedPost.Image);
            }
        }

        [Fact]
        public void PostRepository_GetAll_ReturnIQueryableOfPosts()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);

                var posts = postRepo.GetAll();

                Assert.IsAssignableFrom<IQueryable<Post>>(posts);
                Assert.Equal(1, posts.Count());
            }
        }

        [Fact]
        public async Task PostRepository_GetByIdAsync_ReturnPost()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);
                int idToGet = 1;

                var post = await postRepo.GetByIdAsync(idToGet);

                Assert.Equal(1, post.Id);
                Assert.Equal("f2b1582b-e36e-44fe-9bae-42f9220d3499", post.UserId);
                Assert.Equal(new DateTime(2021, 9, 9), post.PostedDate);
                Assert.Equal("Hey guys! It's my first post", post.PostText);
                Assert.Equal("iVBORw0KGgoAAAANSUhEUgAAAcgAAADHCAYAAACdgMOLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABoLSURBVHhe7d07juM4HsdxH8WeI/gaiif2GewTbOaJOhIqmQsUGlhnbhgdLNCdTw8qcCWVzEzSJygsFj0YrkhKFkX/Sb1dkutbwAfdNvWiHvyJsiwvFH/88ccff/zxd/V3Ccg//vgDAADkKgH5+voKAAAyBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAE7QPyt41a/GuhNr8JZQGHx4UZx3g8iMPc1NMPtV78rQ5S2VxE6nDaLdXudP3+bRzVdrVVR7HsHkysfs8PKkke1LNUBqCX0QPy6fM6G36t9n/K5ZLDRqlFtmSlgcPMC5fR5zeGQEA+PyRquTtdXp/TRK0qDfpZpclKrbbHyzDd2Olsj/77fQIkNM06eZ1WXcYNmVD9zqlKsrrp+q1WiUrP1XJ9QuRucwDDyOLA/o0VkLb3uGkVODqwNge5bBBCQI46vzFIAal7E8udOrnvZY7bMhBNYCapOnvDtDeVANHz06HRMXyCplI/PU45P3n7ndRumaiHZ/c9AH01C8g8FF2XgPxzr9bO++vPT5VxBw9IEwxlb68yXIsyt5fYdX5mvL1brqf5X7VfC9M7/K0W6x/qKR+vXI4m08zLI3XQdE8ieXi+vC7pRj1rmFPdE7nugbRle6VFj8bv2eQBctxeypL0XI5f6Q2VYRGfZh7yTtl1yAwXkNOsX87M9zqg/SsHF6edWi7f8pI7MF9ZM2v/ggGZB+Al+Co9yIPaBMrspdUyOK1ml1r9ACmC5fX1f2pTCQX9+h+1f2pS5gSRCZpy2OD8zHDFNK5fm/Euy5aHWzYP/e96/9/svXKZnvb/qMXmf3Y6LmlZhGnW1aGuF1E00JXGvJdYDytr5ItLuJUG3e996dduYDcMORNCfkgMF5DW1Opnme0oXR4PXD0gIIHusibX/oUC8uozRDcghZ6l5vYiB+1BmlBwgszIAytW5vTeyulUQ0manxRq7rCx8UxAZvNdr/8xw1ze08Po5aksZ/2y1NXBBqTQQOYuPZjenz0WYgHiNO5uY+/1rqyGAeL02Cw/QG4ZkG9Rv2IY4X2Dy6zA0LLm2f71CcjL5VbBoAHph0TXspED0swvG+9p/7faH7J5Zf8vh3V7tpmGy9IrIE3DnTXUR9uADxMiHQJEN/DRzz9j03SCxp3mxQQCcsT62RMcv84uAhIYWm1AVkPwSe1T20s0r4vPHyNf3Rj2M0jvMmPTMhMmRSjZzwgb9doq412/jo63/lttNjrQsvltsv9nPUkznjCN9styXYdwA2kb5eLSqmlo/Ua842U4/bnZ9SXbSICYsniIidM00ygDxH5e54dFJCBnXr/6cMxwiRUYXH1AZmzI5cH42Ybipdfo3aTjf8449k06170qucz0BvP3N4fys0FdFp2f7rldpumEZSY8ng3r4pKqnXd1fsU01/sfjZclVgdNuknHNLaVQLSNeOVSq2lcO3xVwDTsujHXigY+FiD+OBk/rMVpFqFhJWnqzCOvj6cSUnOunz+9nB/C3KQDDC9rau1fLCAxE6FeRC3d+7znRvQ91I/Lq8DQCMg7E+xJBJjhs/CQvx4yf/deP40HBQDjICDv0Ns+ag43pS+hEo7AKAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBA3tyL2vz8tdWDE4bU6g5X/b3K4I/xPquHZGm+QtHqi+j5F9cNfugXwIQRkDf3dgHpf0fy+hFm9nFt7lN2wt+x0wEZ+HL6X3u1/mWhFsZa7f8ShomGLwC8PQLy5t4oIANP2TGPOMsDUXxOa/ApLaGAPKhNFoyb3/PXJiyFRw0SkAAmbnIBqRtp+yzKrOHOGus0f0Zl7AHQ/dhna6amN2XnVT5MOlZW4+WbWv/8US1y68fveVkekF++CmWv6vChHEfbfCmmacfbP34Sx/PnV45nSc9pteyzTGM/piw/nScQkL9v1OLXffkM3P+sTU/yEpgFAhLAxE0yIO1lP9tw60CyoXkWh784/1CfflLqo+drbbDa+VwuK1Z+c88rMw+OlkPEp4PODylLB10WYh9e7GsTlIEepQm9oswbz5R9UvuXosydhn5dlGnxZ3XadR4Jf7H3KQekCcR/2193Ofx7YcJyn/27/k/5G6EGAQlg4qYZkCaQdDjZMGoUkJ3p+fi/xuAG5PVndE16s09FT68ItAsvzCohmHF6lpYbkG4Iflf7XR7CXu/R8gMy/BDzIiDdzx6rpICNB6QJRycoCUgAc3M/AdmrBzl8QF4UgXcJylhAej2/q7JAQOp57L6VP/t1JRKQRa84+mPKzQPSXGL9xQ3EJ7X/lUusAOaHHmSbgKxcfs01+b29WNC5Zeb/ZUDazyMD41Uuzeqy0CVdLXSJtcGPKWstLrHam3ScO1dNYHKTDoD5ISBrA9LenGPZ5XHHL35OqRqQtndneo65MrwiAZlxb9JZP35zhrUhWE7TvYRaTMcp93qU0k06jX5MOdPqJh2Nr3kAuAOTC8hp8cLzTXnB2pbYC2wi1PuMBGQTBCSAiSMgo+4oIDNyTzCu04MCmiAgAUwcARl1XwGptXrUXPTHeHVA5o+Mq/sM1sWj5gDMBAEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCRKL9/U+uev6iCUnXZLtTtdv38vYvV787o/P6gkeVDPbcvuxTlVyWqltkehbCrew3Z4h2YYkGeVJiu1mvoBM0eBgHx+SNRyd6q8d09i9btd3Z/U/teFWvyyUJvfr8t1SIeWI1bW1jlNsmNrq46X9/LjbXusDHdTkwnI7tsI8zSzgDyq7SpR6dketATkwKSA1GfGy506ue/dk1j9blb3g9r8slb7v2wDLDW+r68ntVsm6uG5bVl7x20ZiCYwk1SdvWHen77bCHM0yYA0B2h2xli4DsJbBaQO5K1KzVm1XZYkPTcoizt8+KgWP5c2X4qyF7XJAmr/+OlStn783qAsY8Kt7TSvx1t4AanPipOH53J4Y5z1MlQdyvGuyyrjZeT6hct0YCRpmtUxq1sWHGm+rw6zL8Ya31v2dPU2zOqU1TMxJ6TSMO0Ej+nj1lt/dt7FPuOOd7WO855lUd54P+ul+zbC/Ey/B2kOAveSj9YyIM8/1KeflPro+Vo7vj1YL5eXzMFcLItXZpazQ2NiGvAilHQQZA35hxen7JPavzQpc4NNvw6M9+VrFhTV+V0CpbIsWuiMeIT1UqmP/7quDqG625ORSmBWtO+VlZcgy4bchuYQjXO88b1lb9fWc6TQ8Y9pZ//RgXg9T/l418M2bgMKnduCQo9thNmZZkDmZ5Ul52AybtuDvMy7cmB7ZW2WyTTwtkdjhRr772q/Kxr4SJkJE3d6WiA83RDUy7H7pp6kMkOHhHSwD79ennTvsAjAXBlukTpE655PV7/nTdsK1S9cZoLDhL+upw3+mwVkh0DvqgjIy4lOXzXHdHx+8j40+DI20mcbYW4mGJBlw2Ne+2ebRsuA7NWDHDYI/B5ONZQ6BqQfdBV3HpDRujuKk5LKPEL1C5e9i4A02zOr21H/2+I4CyrXlXktHNNmHSbFui3ft2r2oSJ8mwTl2D3IIbcD3tz0ArI4OPODyX4GUT2YWgdkZy2CoHKZMXfaqeXS+4qAadyrlwCDPUjTqDctK8LEFwmXyrLYwC2nqYUO9vHXS/V1pA7Runsq42ntA6dRQEr1a6TH5btQWetlscdWUR9TX/8mnbbTrDum9T5i5lGdd6nB8S6E7jh6bCPMzuRv0rE3RLiNb1lWGC8o64LAXQ7nDDlnPrAXGhL3Jp314zen8beNfVHmXiqMl2VM4++UX3pVsXBxLkFmNl+8YTOxm3SGXi+XXp7h1z1ch3Ddi9Av+UHa5Sad2oA0jWSbW/71HZL26wMuvxHudJNOy2Uxx14lEPPt6fbOWtcvckybEyhnHzH7UhGSNhjLfcmyx/t12bgnzP23EeZn+jfpTJYXEoO4DqhmZSNqfUY8xnoZUax+rete0L3PLj3ImPa93bJsjGUZepr3ILYdMEcEZGfvJCAz7c6KZxaQmU49swAzfBYeoV5pV7o3G1qOUNkYyzJW/e5BbBthngjIzt5PQGr64G/WY5hfQGqx+jWv+0j0Z36hhjdWhtthO9wlAhIAAAEBCQCAgIAEAEBAQAIAICAgAQAQEJAAAAgISAAABAQkAAACAhIAAAEBCQCAgIAEAEBAQAIAICAgAQAQEJAAAAgISAAABAQkAAACAhIAAAEBCQCAgIAEAEBAQAIAICAgAQAQEJAAAAgIyLf28k2tf/6qDlJZR6fdUu1OctnbOqrtaquOYtkbeH5QSfKgnqUyAO/e/ALynKpktVIrI1HpWRhmTloF5He1331Umy9SmfX8kKjl7iSWyc4qTez63B6l8i7sNK+n1ycgQ9OsE6+fPplot74AvBczC0jd2JUN7DlN1CpJ1bkyzMwMGZC6R7TcqZNUJtKBpU8yuoZPyFQCskn9Tmq3TNTDs1QG4D2bYEDaxsz2EAuBRvW4DZcNwjboqQ7ifFmS9NygrIYJxY9qceEEpFdWhOHT4ydn+MIntX/Jx8vo3lDy8Hx5XThu3XUpBcVwAWlOWpx5WUVPPw9Is91sWWWdVa4OlMsTn2b/+rXvdQN4Dybfgzxuw5dRTcO5PYplFecf6tNPSn30fK0NBN2gZ41uMY9KIHtlpnEPL2vpRW2c4Kv0IM3/ndDzX0d7kA17QmY5/ZOK4QLSCk2vbn26y6Vfu+uz4TJ2qV/rnjeA92DSAakDMNiojd571LxGu9L4+g16wwb8y1e12H1TT8VrJyBNL/HDS2X4wwc3EOsCMtDIOz02q2WAtBYLyMD6NP93l1FrGJC968dlVgDXphuQutEL9A7tJTe/EYzo1YMMNOizCUi9nE7QdOlhtdYhIPX2jn6eHJtm3/oRkACuTTQgs0Yv0Fi2DsdeIg26Xyb1aE87tVx6X7moXDa1gXf5DLL2EqsNzPXj98vrUqCRN8tcBoj9vM5ff5EAkerQgJ7P9WeydeszHtLiNPvWT+MSKwDBTG7SyRtA0xj6ZfFGtZ/6Br1cDqcXkzM3fwjh4t5ws/miP5N0btLRPczATTiGCU25vMlNOkmaOnXy62BV1qcJjw5fhahsq2LdxNanP07GP0kSp9mzfhlu0gEgmfxNOtPlNfZTMEpPSPdMp/rggSFweRWAjIDsbIIBmRmyN1T0gKVe6b3gQQEAQgjIzqYZkNp0HzU3MfrzVcIRQAABCQCAgIAEAEBAQAIAICAgAQAQEJAAAAgISJScx975Zfd+Z2ysfm9ed/391tAPO8fK7pr3gI2Iwbbf1NZ1/uCM8R6UMoCZ75/zC8jA01QwgEBA3vuTZmL1u13dn9T+14Va/LJQm9+vy3UjH1qOWNl01PyWaWvNArL19qtpXya1ricTkN333ambWUDqx9CV3z00z2Wd+w8mT4kUkKM8nWdCYvW7Wd0PavPLWu3/sg2N1MjEn/gzh6cBvUFAtt5+TdoXnrxU1XffnbZJBqT7bE3p2ZkX0gPCB2UfBjD0DybrB46Xz1t1Gw170O+dZ7WWDyaPlWUqz2htOs3r8So/3pzRZ3/XT9IZZ70MVYdKI+yV+Q96l+sXLtONpn3ea1a3rPFM8311mLP4WCNzq57u8Nu27se++xwPB+fZxc22rfSs50AbEmhfhryqEGzrzLzd/Uqv+3J9u+Nd7XuVXnCL46+X7vvulE2/B2k2trwDmzO82f1gssc04EUo6YM+O9iLn7wyZUVDUlfmBpt+HRjPNCjV+V0apMqyaKEzvxHWS6U+/uu6OoTq7v9cmK99r6z8NZmywbKhOUQjFG9kbtPbHWufb9iDrOyD9fuuXKY167V0+kH2sa4s+G2ds+51IF7vY/Kv1OhhW5+wdW4jCz323QmbZkDmZ08lZ6epDCO8PyjdIDjzqOzAXllgZxU5Z71WqLF3G5VImWlU3OlpgfB0GyC9HIHfprTz0I2MtFMPv17iv4UZqUO07k7vxZu2FapfuKxsNHU9beN6s4CMNvrNAqHeSPt8LCCbHg+V/TNWpsW2raW3W3DZo+3LUOs6U9PWmf1Nvy8FdWD9x8cZS599d7omGJBlw2Ne+2dVGbsDhHZewZR+MNkc2H5PKXDQNw1IP+gqIg3JPQRktO6OohGuzCPWiMplZt8jIPNhhwjIFsdD4zIttm0zOpgCAVLfvgy5ruvbuiQp9rnyfatm/Rfh2yQox+5BDrbObmt6AWl2knKnsdfay52mdTj20qKxkM44a38wufj8JXDQm0a9aVng7DzWkFSWxTZg5TS10E49/nqpvo41hrG6eyrjabGDVi5rFJAdf2S612WqUFnrZRlh2+b0vu5/Tuhv9+jxcLXdQ2VabNtm9ejzg+xDreuats6sX7OcNgibXmKtqGy/MfXYdyds8jfpVH4A12zssqzQ7Ay2i7rGwl0O50wwZz6YFg4Y2whY68dvzoFuG/uirPqDyLGyjGkgnPJLryrekER/vDkTu0ln6PVy6eUZft3DdQjXvQj9kh+kXW7SqQ1I0xi0ubVd3wlob5N3+Y1N7EaHYFnrZRlp22qV7eSHonV9PIS2e80+kWl+k05ej4bty3DrOtLWmZMPZ/3my2b3M6kOxXJel43XPmr9990pm/5NOpPlNSSDuA6oZmUjan3mN8Z6GVGsfq3rXtC9lxY9iUba93bLsqGW5Y62bWe3Wtf3JLbOpo2A7OydBGSm3dnfzBrRTKx+bc98zfBZIxnqlXale0Oh5QiVDb8s97Vtu7jdur4fsX136gjIzt5PQGp6J292Zjy/RlSL1a953UeiP9sKNTCxssHd37Zt5abr+k7MfJ0RkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAvrWXb2r981d1kMo6Ou2WaneSy97WUW1XW3UUy97A84NKkgf1LJUBePdmF5DH7UqtVoVEpWd5uNloFZDf1X73UW2+SGXW80OilruTWCY6pyoZfH2eVZqs1Pbov98nIEPTrFFTP30y0Wp9AXg3Zt2DPKeJWiWpOgtlszFkQOoe0XKnTlKZSIdOGVjDrc+pBGST+p3Ubpmoh2f3PQCYaEBWe4nhRtE0eNujWDYM26Cnej75siTpuUFZDROKH9XiwglIr6wIw6fHT87whU9q/5KPl9G9oeTh+fK60HR9vh63WXm/S6BmmzjzsoqeWx6QZj62rLLOKr29cjnj0+xfv9a9bgDvwvR7kKbRdBs13cjmjWHT3s75h/r0k1IfPV9reyP5vIoQrjSwXplZziaXKF/Uxgm+Sg/S/N8JPf91tAfZsCd0tT5Lw51wxHqQsfXpb2d3fTbsQXapX+ueN4D3YJoB6fQwLLnBG6LHE+c12pXG12/QGzbgX76qxe6beipeOwFpeokfXirDHz64gVgXkIFGvsn6HHRdxgIysD7N/91l1BoGZO/6cZkVwLUJBqTXc4j0CK57GQG9epCBBn02AVm/Pu0lzNA67qJDQOoAi14RiE2zb/0ISADXpheQpoErGzz7+VKgcattVPtqEZBSD+W0U8ul95WLymVTG3iXzyBrL7HawFw/fr+8LgUa+Zr1WRseUh0a0PO5/ky2bn3GTzDEafatn8YlVgCCyd+kk6Sp06jaRvRyKW3UcNTqG/TLsri9mJy5+UMIF/eGm80X/Zmkc5OO7mEGbsIxTGjK5U1u0qmsT1Mftw5WJaRMeHT4KkRl2sW6ia1Pf5yMv33FafasX4abdABIpn+TzmR5jf0UjNIT0j3TqT54YAhcXgUgIyA7m2BAZobsDRU9YKlXei94UACAEAKys2kGpDbdR81NjP58lXAEEEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAICAgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgAAAQEJAIDgzgLySe3ThVo8HoSyKTmrNFmp1WqltkepfGDnVCXZvPT8VqtEpWdhmAEdt8W8bjM/ABgDAXlzR7U1oWFDcvyA1PPZqmP++pwmapWk6lwZZjy3nh8ADGWCAWlDbv35ST19XqvFv7LA+9dGHYryP/dqbd6z9HD6/cNj+V5prfZ/luMUw77+tjHlm99q5pePt3m0wxsNw7fai5KCcNiArJ9f7rjNysvA7Krp/ExAbo9iGQBM2XQDMguljQ60Srgd1Oby/2zYStCV416FWJOAlOaX/78ITBugeei6069jLnH6oTRiD1KcnzVKYF3NT/eS8/Ck9whgpiYbkJVeYyEPNt8l+HoEpDi/6Hg1TE/N7WWNHJC18yuGkYOztSbzuww30DwB4IamG5DSpczagOoRkNL8Ogdk8Tlj/nr0HmT9/EzPcbCgalK/gjcsAMzEvAKyuOQplRn5uOlePbnvV4IuH2bMgDSBUYaC/byuRUCedmq5XKrdSSiT1MyvNhwHnl+F7kFymRXADM0rILUiJC+8zwTzEPPL3Jt4Np/tNMbrQVZvYknSNOtFFQHifD7nqATl84NKssBa7k7lezWC8zNhVp2XNtr8/PoRjgBmaoIBidfXk9q16dH1duv5AcD0EZAT8/yQmMudycOzWD60W88PAOaCgAQAQEBAAgAgICABABAQkAAACAhIAAAEBCQAAAICEgCAK6/q/1Hx349dHw2pAAAAAElFTkSuQmCC", post.Image);
            }
        }

        [Fact]
        public async Task PostRepository_GetByIdWithRelatedAsync_ReturnPostWithRelatedUsersAndLikes()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);
                var expectedId = "f2b1582b-e36e-44fe-9bae-42f9220d3499";
                var expectedCountLikes = 1;
                int idToGet = 1;

                var post = await postRepo.GetByIdWithRelatedAsync(idToGet);

                Assert.Equal(expectedId, post.User.Id);
                Assert.Equal(expectedCountLikes, post.Likes.Count);
            }
        }

        [Fact]
        public async Task PostRepository_GetAllWithRelated_ReturnPostWithRelatedUsersAndLikes()
        {
            using (var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var postRepo = new PostRepository(context);
                var expectedId = "f2b1582b-e36e-44fe-9bae-42f9220d3499";
                var expectedCountLikes = 1;
                int idToGet = 1;

                var post = await postRepo.GetByIdWithRelatedAsync(idToGet);

                Assert.Equal(expectedId, post.User.Id);
                Assert.Equal(expectedCountLikes, post.Likes.Count);
            }
        }
    }
}
