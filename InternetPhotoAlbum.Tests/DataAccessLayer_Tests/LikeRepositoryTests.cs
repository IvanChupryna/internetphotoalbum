﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Repositories;
using InternetPhotoAlbum.Tests.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace InternetPhotoAlbum.Tests.DataAccessLayer_Tests
{
    public class LikeRepositoryTests
    {
        [Fact]
        public async Task LikeRepository_Remove_RemovesLikeFromDatabase()
        {
            using(var context = new PhotoAlbumContext(DbCreator.GetDbContext()))
            {
                var likeRepo = new LikeRepository(context);
                var likeToRemove = context.Likes.First();
                int expectedLikesCount = 0;

                likeRepo.Remove(likeToRemove);
                await context.SaveChangesAsync();

                Assert.Equal(expectedLikesCount, context.Likes.Count());
            }
        }
    }
}
