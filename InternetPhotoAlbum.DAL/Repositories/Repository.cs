﻿using InternetPhotoAlbum.DAL.Entities.Abstract;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        readonly DbSet<TEntity> entities;

        protected Repository(DbContext context)
        {
            entities = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            entities.Add(entity);
        }

        public async Task AddAsync(TEntity entity)
        {
            await entities.AddAsync(entity).AsTask();
        }

        public IQueryable<TEntity> GetAll()
        {
            return entities;
        }

        public TEntity GetById(int id)
        {
            return entities.Find(id);
        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await entities.FindAsync(id);
        }

        public async Task RemoveByIdAsync(int id)
        {
            var entityToDelete = await GetByIdAsync(id);
            entities.Remove(entityToDelete);
        }

        public void Update(TEntity entity)
        {
            entities.Update(entity);
        }
    }
}
