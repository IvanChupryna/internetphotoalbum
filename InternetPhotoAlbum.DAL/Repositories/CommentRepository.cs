﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        readonly PhotoAlbumContext _photoAlbumContext;

        public CommentRepository(PhotoAlbumContext photoAlbumContext) : base(photoAlbumContext)
        {
            _photoAlbumContext = photoAlbumContext;
        }
        public IQueryable<Comment> GetAllWithRelated()
        {
            return _photoAlbumContext.Comments.Include(c => c.Post)
                .Include(c => c.User);
        }

        public Comment GetByIdWithRelated(int id)
        {
            return GetAllWithRelated().FirstOrDefault(c => c.Id == id);
        }

        public async Task<Comment> GetByIdWithRelatedAsync(int id)
        {
            return await GetAllWithRelated().FirstOrDefaultAsync(c => c.Id == id);
        }
    }
}
