﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories
{
    public class UserProfileRepositry : Repository<UserProfile>, IUserProfileRepository
    {
        readonly PhotoAlbumContext _photoAlbumContext;

        public UserProfileRepositry(PhotoAlbumContext photoAlbumContext) : base(photoAlbumContext)
        {
            _photoAlbumContext = photoAlbumContext;
        }

        public async Task<UserProfile> GetUserProfileByUserIdAsync(string userId)
        {
            return await _photoAlbumContext.UserProfiles.FirstOrDefaultAsync(uP => uP.PhotoAlbumUserId == userId);
        }
    }
}
