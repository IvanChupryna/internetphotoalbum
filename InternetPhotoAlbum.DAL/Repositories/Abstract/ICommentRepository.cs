﻿using InternetPhotoAlbum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories.Abstract
{
    public interface ICommentRepository : IRepository<Comment>
    {
        IQueryable<Comment> GetAllWithRelated();
        Comment GetByIdWithRelated(int id);
        Task<Comment> GetByIdWithRelatedAsync(int id);
    }
}
