﻿using InternetPhotoAlbum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories.Abstract
{
    public interface IUserProfileRepository : IRepository<UserProfile>
    {
        Task<UserProfile> GetUserProfileByUserIdAsync(string userId);
    }
}
