﻿using InternetPhotoAlbum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories.Abstract
{
    public interface ILikeRepository : IRepository<Like>
    {
        IQueryable<Like> GetAllWithRelated();
        Like GetByIdWithRelated(int id);
        Task<Like> GetByIdWithRelatedAsync(int id);
        void Remove(Like like);
    }
}
