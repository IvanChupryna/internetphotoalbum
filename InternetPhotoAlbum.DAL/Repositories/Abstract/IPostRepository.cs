﻿using InternetPhotoAlbum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories.Abstract
{
    public interface IPostRepository : IRepository<Post>
    {
        IQueryable<Post> GetAllWithRelated();
        Post GetByIdWithRelated(int id);
        Task<Post> GetByIdWithRelatedAsync(int id);
    }
}
