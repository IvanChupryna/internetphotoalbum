﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories
{
    public class PostRepository : Repository<Post>, IPostRepository
    {
        readonly PhotoAlbumContext _photoAlbumContext;

        public PostRepository(PhotoAlbumContext photoAlbumContext) : base(photoAlbumContext)
        {
            _photoAlbumContext = photoAlbumContext;
        }
        public IQueryable<Post> GetAllWithRelated()
        {
            return _photoAlbumContext.Posts.Include(p => p.Comments)
                .Include(p => p.Likes)
                .Include(p => p.User);
        }

        public Post GetByIdWithRelated(int id)
        {
            return GetAllWithRelated().FirstOrDefault(p => p.Id == id);
        }

        public async Task<Post> GetByIdWithRelatedAsync(int id)
        {
            return await GetAllWithRelated().FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}
