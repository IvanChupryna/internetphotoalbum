﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.Repositories
{
    public class LikeRepository : Repository<Like>, ILikeRepository
    {
        readonly PhotoAlbumContext _photoAlbumContext;
        public LikeRepository(PhotoAlbumContext photoAlbumContext) : base(photoAlbumContext)
        {
            _photoAlbumContext = photoAlbumContext;
        }
        public IQueryable<Like> GetAllWithRelated()
        {
            return _photoAlbumContext.Likes.Include(l => l.Post)
                .Include(l => l.User);
        }

        public Like GetByIdWithRelated(int id)
        {
            return GetAllWithRelated().FirstOrDefault(l => l.Id == id);
        }

        public async Task<Like> GetByIdWithRelatedAsync(int id)
        {
            return await GetAllWithRelated().FirstOrDefaultAsync(l => l.Id == id);
        }

        public void Remove(Like like)
        {
            _photoAlbumContext.Likes.Remove(like);
        }
    }
}
