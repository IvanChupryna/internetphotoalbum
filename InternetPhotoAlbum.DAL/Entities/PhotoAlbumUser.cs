﻿using InternetPhotoAlbum.DAL.Entities.Abstract;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.DAL.Entities
{
    public class PhotoAlbumUser : IdentityUser
    {
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
