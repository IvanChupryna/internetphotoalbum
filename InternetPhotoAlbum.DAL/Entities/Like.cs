﻿using InternetPhotoAlbum.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.DAL.Entities
{
    public class Like : BaseEntity
    {
        public string UserId { get; set; }
        public int PostId { get; set; }
        public virtual PhotoAlbumUser User { get; set; }
        public virtual Post Post { get; set; }
    }
}
