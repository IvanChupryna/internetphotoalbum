﻿using InternetPhotoAlbum.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.DAL.Entities
{
    public class Post : BaseEntity
    {
        public string Image { get; set; }
        public string UserId { get; set; }
        public string PostText { get; set; }
        public DateTime PostedDate { get; set; }
        public virtual PhotoAlbumUser User { get; set; }
        public virtual ICollection<Like> Likes { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
