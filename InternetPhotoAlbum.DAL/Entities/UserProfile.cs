﻿using InternetPhotoAlbum.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace InternetPhotoAlbum.DAL.Entities
{
    public class UserProfile : BaseEntity
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhotoAlbumUserId { get; set; }
        public virtual PhotoAlbumUser PhotoAlbumUser { get; set; }
    }
}
