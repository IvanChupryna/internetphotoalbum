﻿using InternetPhotoAlbum.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.DAL.DBContexts
{
    public class PhotoAlbumContext : IdentityDbContext<PhotoAlbumUser, PhotoAlbumRole, string>
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Like> Likes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public PhotoAlbumContext(DbContextOptions<PhotoAlbumContext> options) : base(options) { }
    }
}
