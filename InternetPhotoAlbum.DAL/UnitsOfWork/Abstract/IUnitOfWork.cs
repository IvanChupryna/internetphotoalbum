﻿using InternetPhotoAlbum.DAL.Repositories;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.UnitsOfWork.Abstract
{
    public interface IUnitOfWork<TUser, TRole> 
        where TUser : class
        where TRole : class
    {
        UserManager<TUser> PhotoAlbumUserManager { get; }
        RoleManager<TRole> PhotoAlbumRoleManager { get; }
        IUserProfileRepository UserProfileRepository { get; }
        IPostRepository PostRepository { get; }
        ILikeRepository LikeRepository { get; }
        ICommentRepository CommentRepository { get; }
        Task<int> SaveAsync();
    }
}
