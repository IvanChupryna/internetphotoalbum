﻿using InternetPhotoAlbum.DAL.DBContexts;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.Repositories;
using InternetPhotoAlbum.DAL.Repositories.Abstract;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.DAL.UnitsOfWork
{
    public class UnitOfWork : IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole>
    {
        readonly PhotoAlbumContext _photoAlbumContext;
        readonly UserManager<PhotoAlbumUser> _userManager;
        readonly RoleManager<PhotoAlbumRole> _roleManager;
        IUserProfileRepository _userProfileRepository;
        IPostRepository _postRepository;
        ILikeRepository _likeRepository;
        ICommentRepository _commentRepository;

        public UnitOfWork(PhotoAlbumContext photoAlbumContext, UserManager<PhotoAlbumUser> userManager, RoleManager<PhotoAlbumRole> roleManager)
        {
            _photoAlbumContext = photoAlbumContext;
            _userManager = userManager;
            _roleManager = roleManager;
        }
        
        public UserManager<PhotoAlbumUser> PhotoAlbumUserManager
        {
            get
            {
                return _userManager;
            }
        }

        public RoleManager<PhotoAlbumRole> PhotoAlbumRoleManager
        {
            get
            {
                return _roleManager;
            }
        }

        public IUserProfileRepository UserProfileRepository
        {
            get
            {
                if(_userProfileRepository == null)
                {
                    _userProfileRepository = new UserProfileRepositry(_photoAlbumContext);
                }
                return _userProfileRepository;
            }
        }

        public IPostRepository PostRepository
        {
            get
            {
                if (_postRepository == null)
                {
                    _postRepository = new PostRepository(_photoAlbumContext);
                }
                return _postRepository;
            }
        }

        public ILikeRepository LikeRepository
        {
            get
            {
                if (_likeRepository == null)
                {
                    _likeRepository = new LikeRepository(_photoAlbumContext);
                }
                return _likeRepository;
            }
        }

        public ICommentRepository CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                {
                    _commentRepository = new CommentRepository(_photoAlbumContext);
                }
                return _commentRepository;
            }
        }


        public void Save()
        {
            _photoAlbumContext.SaveChanges();
        }

        public async Task<int> SaveAsync()
        {
            return await _photoAlbumContext.SaveChangesAsync();
        }
    }
}
