﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.BLL.Validation;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> _uow;
        private readonly IMapper _mapper;

        public PostService(IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }


        /// <summary>
        ///     Asynchronously adds object of type <see cref="PostDTO"/>
        ///     to the Database   
        /// </summary>
        /// <param name="dto"><see cref="PostDTO"/> object to add</param>
        public async Task AddAsync(PostDTO dto)
        {
            PhotoAlbumValidator.ValidatePost(dto);

            await _uow.PostRepository.AddAsync(_mapper.Map<Post>(dto));
            await _uow.SaveAsync();
        }


        /// <summary>
        ///     Asynchronously removes object of type <see cref="PostDTO"/> from the Database
        /// </summary>
        /// <param name="dto"><see cref="PostDTO"/> object to update</param>
        public async Task RemoveByIdAsync(int id)
        {
            await _uow.PostRepository.RemoveByIdAsync(id);
            await _uow.SaveAsync();
        }


        /// <summary>
        ///     Gets all available objects of type <see cref="PostDTO"/>
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of type <see cref="PostDTO"/></returns>
        public IEnumerable<PostDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<PostDTO>>(_uow.PostRepository.GetAll());
        }


        /// <summary>
        ///     Searches for posts by given date
        /// </summary>
        /// <param name="date"></param>
        /// <returns><see cref="IEnumerable{T}"/> of type <see cref="PostDTO"/></returns>
        public IEnumerable<PostDTO> GetByDate(DateTime date)
        {
            if(date > DateTime.Now)
            {
                throw new PostException("Given Date is invalid");
            }

            return _mapper.Map<IEnumerable<PostDTO>>(_uow.PostRepository.GetAll()
                                                    .Where(p => p.PostedDate == date));
        }


        /// <summary>
        ///     Asynchronously gets object of type <see cref="PostDTO"/>
        /// </summary>
        /// <param name="id">Id of the searched <see cref="PostDTO"/></param>
        /// <returns><see cref="PostDTO"/> object or null</returns>
        public async Task<PostDTO> GetByIdAsync(int id)
        {
            return _mapper.Map<PostDTO>(await _uow.PostRepository.GetByIdAsync(id));
        }

        /// <summary>
        ///     Searches for posts by username of the <see cref="PhotoAlbumUser"/> that posted them
        /// </summary>
        /// <param name="userName"></param>
        /// <returns><see cref="IEnumerable{T}"/> of type <see cref="PostDTO"/></returns>
        public IEnumerable<PostDTO> GetPostsByUsername(string userName)
        {
            var postUser = _uow.PhotoAlbumUserManager.FindByNameAsync(userName).Result;
            var postsToShow = _uow.PostRepository.GetAllWithRelated()
                .Where(p => p.UserId == postUser.Id);

            if(postsToShow == null)
            {
                throw new PostException("There's no such user");
            }
            return _mapper.Map<IEnumerable<PostDTO>>(postsToShow);
        }


        /// <summary>
        ///     Gets Posts sorted by posted date
        /// </summary>
        /// <returns>
        ///     <see cref="IEnumerable{T}"/> of type <see cref="PostDTO"/>
        ///     sorted by posted date
        /// </returns>
        public IEnumerable<PostDTO> GetSortedByDate()
        {
            return _mapper.Map<IEnumerable<PostDTO>>(_uow.PostRepository.GetAll()
                .OrderByDescending(p => p.PostedDate));
        }

        /// <summary>
        ///     Gets Posts sorted by likes count
        /// </summary>
        /// <returns>
        ///     <see cref="IEnumerable{T}"/> of type <see cref="PostDTO"/>
        ///     sorted by likes count
        /// </returns>
        public IEnumerable<PostDTO> GetSortedByLikes()
        {
            return _mapper.Map<IEnumerable<PostDTO>>(_uow.PostRepository.GetAllWithRelated()
                .OrderByDescending(p => p.Likes.Count));
        }


        /// <summary>
        ///     Asynchronously updates object of type <see cref="PostDTO"/>
        /// </summary>
        /// <param name="dto"><see cref="PostDTO"/> object to update</param>
        public async Task UpdateAsync(PostDTO dto)
        {
            PhotoAlbumValidator.ValidatePost(dto);

            _uow.PostRepository.Update(_mapper.Map<Post>(dto));
            await _uow.SaveAsync();
        }
    }
}
