﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services.Abstract
{
    public interface ILikeService
    {
        Task<IEnumerable<LikeDTO>> GetAllLikesOfCurrentPostAsync(int postId);
        Task AddLikeToPostAsync(string userId, int postId);
        Task RemoveLikeFromPostAsync(string userId, int postId);
    }
}
