﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services.Abstract
{
    public interface ICrud<TDto> where TDto : class
    {
        IEnumerable<TDto> GetAll();
        Task<TDto> GetByIdAsync(int id);
        Task AddAsync(TDto dto);
        Task UpdateAsync(TDto dto);
        Task RemoveByIdAsync(int id);
    }
}
