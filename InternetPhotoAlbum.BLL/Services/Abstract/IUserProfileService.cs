﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services.Abstract
{
    public interface IUserProfileService
    {
        Task<UserProfileDTO> GetUserProfileByUserIdAsync(string userId);
        Task<UserProfileDTO> UpdateUserProfileAsync(UserProfileDTO userProfile);
        Task RemoveUserProfileByUserIdAsync(string userId);
    }
}
