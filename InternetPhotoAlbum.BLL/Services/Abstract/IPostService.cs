﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.Services.Abstract
{
    public interface IPostService : ICrud<PostDTO>
    {
        IEnumerable<PostDTO> GetByDate(DateTime date);
        IEnumerable<PostDTO> GetSortedByDate();
        IEnumerable<PostDTO> GetSortedByLikes();
        IEnumerable<PostDTO> GetPostsByUsername(string userName);
    }
}
