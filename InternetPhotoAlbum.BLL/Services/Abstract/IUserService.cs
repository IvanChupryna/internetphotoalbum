﻿using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Utilities;
using InternetPhotoAlbum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services.Abstract
{
    public interface IUserService
    {
        Task<UserManagerResponse> RegisterUserAsync(RegisterDTO register);
        Task<UserManagerResponse> LoginAsync(LogInDTO login);
        IEnumerable<UserDTO> GetAllUsers();
        Task<UserDTO> GetUserByIdAsync(string id);
        Task<UserDTO> GetUserByNameAsync(string username);
        Task<UserManagerResponse> EditUserAsync(UserDTO user);
        Task<UserManagerResponse> DeleteUserByIdAsync(string id);
    }
}
