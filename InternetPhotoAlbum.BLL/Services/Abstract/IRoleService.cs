﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services.Abstract
{
    public interface IRoleService
    {
        Task<RoleDTO> GetRoleByName(string name);
        Task AddAsync(RoleDTO dto);
        Task UpdateAsync(RoleDTO dto);
        Task RemoveByNameAsync(string name);
    }
}
