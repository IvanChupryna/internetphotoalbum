﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.Services.Utilities
{
    public class UserManagerResponse
    {
        public bool IsSucceeded { get; private set; }
        public string Message { get; private set; }
        public IEnumerable<string> Errors { get; private set; }
        public DateTime? ExpireDate { get; private set; }

        public UserManagerResponse(bool isSucceeded, string message)
        {
            IsSucceeded = isSucceeded;
            Message = message;
        }

        public UserManagerResponse(bool isSucceeded, string message, IEnumerable<string> errors)
        {
            IsSucceeded = isSucceeded;
            Message = message;
            Errors = errors;
        }

        public UserManagerResponse(bool isSucceeded, string message, DateTime expireDate, IEnumerable<string> errors = null)
        {
            IsSucceeded = isSucceeded;
            Message = message;
            Errors = errors;
            ExpireDate = expireDate;
        }
    }
}
