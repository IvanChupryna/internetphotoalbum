﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.BLL.Validation;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services
{
    public class LikeService : ILikeService
    {
        private readonly IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> _uow;
        private readonly IMapper _mapper;

        public LikeService(IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }


        /// <summary>
        ///     Asynchronously adds <see cref="Like"/> to <see cref="Post"/>
        /// </summary>
        /// <param name="userId">Id of the <see cref="PhotoAlbumUser"/> who likes post</param>
        /// <param name="postId">Id of the <see cref="Post"/> to like</param>
        /// <exception cref="LikeException"></exception>
        public async Task AddLikeToPostAsync(string userId, int postId)
        {
            var currentLike = _uow.LikeRepository.GetAll()
                .Where(l => l.PostId == postId && l.UserId == userId)
                .FirstOrDefault();

            if(currentLike != null)
            {
                throw new LikeException("You've already liked that post");
            }

            currentLike = new Like()
            {
                UserId = userId,
                PostId = postId    
            };

            await _uow.LikeRepository.AddAsync(currentLike);
            await _uow.SaveAsync();
        }


        /// <summary>
        ///     Asynchronously gets all objects of type <see cref="Like"/>
        ///     from <see cref="Post"/> using its Id
        /// </summary>
        /// <param name="postId">Id of the <see cref="Post"/></param>
        /// <exception cref="PostException"></exception>
        /// <returns><see cref="IEnumerable{T}"/> of type <see cref="LikeDTO"/></returns>
        public async Task<IEnumerable<LikeDTO>> GetAllLikesOfCurrentPostAsync(int postId)
        {
            var currentPost = await _uow.PostRepository.GetByIdWithRelatedAsync(postId);
            if(currentPost == null)
            {
                throw new PostException("There's no post with such Id");
            }

            return _mapper.Map<IEnumerable<LikeDTO>>(currentPost.Likes);
        }


        /// <summary>
        ///     Asynchronously removes <see cref="Like"/> from <see cref="Post"/>
        /// </summary>
        /// <param name="userId">Id of the <see cref="PhotoAlbumUser"/> who wants to remove like from  post</param>
        /// <param name="postId">Id of the <see cref="Post"/> from which like will be removed</param>
        /// <exception cref="LikeException"></exception>
        public async Task RemoveLikeFromPostAsync(string userId, int postId)
        {
            var likeToDelete = _uow.LikeRepository.GetAll()
                .Where(l => l.PostId == postId && l.UserId == userId)
                .FirstOrDefault();

            if(likeToDelete == null)
            {
                throw new LikeException("You haven't liked this post yet");
            }

            _uow.LikeRepository.Remove(likeToDelete);
            await _uow.SaveAsync();
        }
    }
}
