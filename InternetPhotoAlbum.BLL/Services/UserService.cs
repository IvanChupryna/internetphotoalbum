﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.BLL.Services.Utilities;
using InternetPhotoAlbum.BLL.Validation;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> _uow;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public UserService(IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> uow, IMapper mapper, IConfiguration configuration)
        {
            _uow = uow;
            _mapper = mapper;
            _configuration = configuration;
        }


        /// <summary>
        ///     Deletes <see cref="PhotoAlbumUser"/> from Database by given Id
        ///     <para>
        ///         Deletes <see cref="UserProfile"/> that is connected to the deleted <see cref="PhotoAlbumUser"/>
        ///     </para>
        /// </summary>
        /// <param name="id">Id of the <see cref="PhotoAlbumUser"/> to delete</param>
        /// <returns>
        ///     <see cref="UserManagerResponse"/> object that contains state of the operation
        ///     and some description about it
        /// </returns>
        public async Task<UserManagerResponse> DeleteUserByIdAsync(string id)
        {
            var userToDelete = await _uow.PhotoAlbumUserManager.FindByIdAsync(id);
            if(userToDelete == null)
            {
                return new UserManagerResponse(false, "There's no user with such Id");
            }
            await _uow.PhotoAlbumUserManager.DeleteAsync(userToDelete);
            await _uow.SaveAsync();

            return new UserManagerResponse(true, "User and connected Profile are successfully deleted");
        }


        /// <summary>
        ///     Edits Username and Email of the given <see cref="PhotoAlbumUser"/>
        ///     <para>
        ///         Edits <see cref="UserProfile"/> that is connected to edited <see cref="PhotoAlbumUser"/>
        ///     </para>
        /// </summary>
        /// <param name="user"><see cref="UserDTO"/> object with edited username and email</param>
        /// <returns>
        ///     <see cref="UserManagerResponse"/> object that contains state of the operation
        ///     and some description about it
        /// </returns>
        public async Task<UserManagerResponse> EditUserAsync(UserDTO user)
        {
            var userToEdit = await _uow.PhotoAlbumUserManager.FindByIdAsync(user.Id);
            var userProfileToEdit = await _uow.UserProfileRepository.GetUserProfileByUserIdAsync(userToEdit.Id);
            string resultMessage = String.Empty;
            if (user.Username != null)
            {
                userToEdit.UserName = user.Username;
                userProfileToEdit.Username = user.Username;
                resultMessage += "UserName was edited successfully\n";
            }
            if(user.Email != null)
            {
                userToEdit.Email = user.Email;
                userProfileToEdit.Email = user.Email;
                resultMessage += "Email was edited successfully";
            }
            if(resultMessage == String.Empty)
            {
                resultMessage = "You haven't changed anything";
            }

            await _uow.SaveAsync();
            return new UserManagerResponse(true, resultMessage);
        }


        /// <summary>
        ///     Gets all available <see cref="PhotoAlbumUser"/> from Database
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of type <see cref="UserDTO"/> 
        /// with data about all users from Database</returns>
        public IEnumerable<UserDTO> GetAllUsers()
        {
            var users = _uow.PhotoAlbumUserManager.Users
                .Include(u => u.UserProfile);
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }


        /// <summary>
        ///     Searches for a user by given id
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <exception cref="UserException"></exception>
        /// <returns><see cref="UserDTO"/> object</returns>
        public async Task<UserDTO> GetUserByIdAsync(string id)
        {
            var userToReturn = await _uow.PhotoAlbumUserManager.FindByIdAsync(id);
            if(userToReturn == null)
            {
                throw new UserException("There's no user with such Id");
            }
            return _mapper.Map<UserDTO>(userToReturn);
        }

        /// <summary>
        ///     Searches for a user by given name
        /// </summary>
        /// <param name="username">Name of the user</param>
        /// <returns><see cref="UserDTO"/> object</returns>
        public async Task<UserDTO> GetUserByNameAsync(string username)
        {
            return _mapper.Map<UserDTO>(await _uow.PhotoAlbumUserManager.FindByNameAsync(username));
        }


        /// <summary>
        ///     Autenticates user to the system
        ///     <para>
        ///         After successful authentication system will generate JW Token
        ///         that contains useful information about current User to help system 
        ///         perform authorization
        ///     </para>
        /// </summary>
        /// <param name="login">
        ///     Data that is used to login user
        ///     <list type="bullet">
        ///         <item>Username</item>
        ///         <item>Password</item>
        ///     </list>
        /// </param>
        /// <returns>
        ///     <see cref="UserManagerResponse"/> object that contains state of the operation
        ///     and some description about it
        /// </returns>
        public async Task<UserManagerResponse> LoginAsync(LogInDTO login)
        {
            var user = await _uow.PhotoAlbumUserManager.FindByNameAsync(login.Username);
            if(user == null)
            {
                return new UserManagerResponse(false, "Such user haven't been found");
            }

            var result = await _uow.PhotoAlbumUserManager.CheckPasswordAsync(user, login.Password);
            if(!result)
            {
                return new UserManagerResponse(false, "Password is not correct");
            }

            var claims = new[]
            {
               new Claim(ClaimTypes.Name, login.Username),
               new Claim(ClaimTypes.NameIdentifier, user.Id),
               new Claim(ClaimTypes.Role, (await _uow.PhotoAlbumUserManager.GetRolesAsync(user)).FirstOrDefault())
           };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["AuthSettings:Key"]));

            var token = new JwtSecurityToken(
                issuer: _configuration["AuthSettings:Issuer"],
                audience: _configuration["AuthSettings:Audience"],
                claims: claims,
                expires: DateTime.Now.AddDays(14),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256));

            string tokenAsString = new JwtSecurityTokenHandler().WriteToken(token);

            return new UserManagerResponse(true, tokenAsString, token.ValidTo);
        }


        /// <summary>
        ///     Adds new <see cref="PhotoAlbumUser"/> with User <see cref="PhotoAlbumRole"/> to the Database
        ///     <para>
        ///         Creates and connects <see cref="UserProfile"/> to the current <see cref="PhotoAlbumUser"/>
        ///     </para>
        /// </summary>
        /// <param name="register">Data that is used to register user
        ///     <list type="bullet">
        ///         <item>Username</item>
        ///         <item>Email</item>
        ///         <item>Password</item>
        ///         <item>Confirm Password</item>
        ///     </list>
        /// </param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <returns>
        ///     <see cref="UserManagerResponse"/> object that contains state of the operation
        ///     and some description about it
        /// </returns>
        public async Task<UserManagerResponse> RegisterUserAsync(RegisterDTO register)
        {
            if(register == null)
            {
                throw new ArgumentNullException(nameof(register), "You haven't provided registration data");
            }
            if(register.Password != register.ConfirmPassword)
            {
                return new UserManagerResponse(false, "Confirm Password is not eqaul to Passwod"); 
            }

            var registeredUser = new PhotoAlbumUser()
            {
                UserName = register.Username,
                Email = register.Email
            };

            var result = await _uow.PhotoAlbumUserManager.CreateAsync(registeredUser, register.Password);

            if (await _uow.PhotoAlbumRoleManager.RoleExistsAsync("User"))
            {
                await _uow.PhotoAlbumUserManager.AddToRoleAsync(registeredUser, "User");
            }


            if (result.Succeeded)
            {
                registeredUser.UserProfile = new UserProfile()
                {
                    Username = registeredUser.UserName,
                    Email = registeredUser.Email,
                    PhotoAlbumUserId = registeredUser.Id,
                    PhotoAlbumUser = registeredUser
                };
                await _uow.UserProfileRepository.AddAsync(registeredUser.UserProfile);
                await _uow.SaveAsync();

                return new UserManagerResponse(true, "User is successfully registered");
            }
            return new UserManagerResponse(false, "User hasn't been registered", result.Errors.Select(e => e.Description));
        }


    }
}
