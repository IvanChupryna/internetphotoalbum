﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> _uow;
        private readonly IMapper _mapper;

        public RoleService(IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }


        /// <summary>
        ///     Asynchronously adds object of type <see cref="RoleDTO"/>
        ///     to the Database, previously mapping it to <see cref="PhotoAlbumRole"/>   
        /// </summary>
        /// <param name="dto"><see cref="PostDTO"/> object to add</param>
        public async Task AddAsync(RoleDTO dto)
        {
            await _uow.PhotoAlbumRoleManager.CreateAsync(_mapper.Map<PhotoAlbumRole>(dto));
            await _uow.SaveAsync();
        }


        /// <summary>
        ///     Gets all available objects of type <see cref="RoleDTO"/>
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/> of type <see cref="RoleDTO"/></returns>
        public IEnumerable<RoleDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<RoleDTO>>(_uow.PhotoAlbumRoleManager.Roles);
        }


        /// <summary>
        ///     Gets object of type <see cref="RoleDTO"/> using its name
        /// </summary>
        /// <param name="name">Name of the searched <see cref="RoleDTO"/></param>
        /// <returns><see cref="PostDTO"/> object or null</returns>
        public async Task<RoleDTO> GetRoleByName(string name)
        {
            return _mapper.Map<RoleDTO>(await _uow.PhotoAlbumRoleManager.FindByNameAsync(name));
        }


        /// <summary>
        ///     Asynchronously removes object of type <see cref="RoleDTO"/>
        ///     from the Database, previously mapping it to <see cref="PhotoAlbumRole"/>
        /// </summary>
        /// <param name="name">Name of the <see cref="PhotoAlbumRole"/></param>
        public async Task RemoveByNameAsync(string name)
        {
            var roleToDelete = await _uow.PhotoAlbumRoleManager.FindByNameAsync(name);
            await _uow.PhotoAlbumRoleManager.DeleteAsync(roleToDelete);
        }


        /// <summary>
        ///     Asynchronously updates object of type <see cref="RoleDTO"/>
        /// </summary>
        /// <param name="dto"><see cref="PostDTO"/> object to update</param>
        public async Task UpdateAsync(RoleDTO dto)
        {
            await _uow.PhotoAlbumRoleManager.UpdateAsync(_mapper.Map<PhotoAlbumRole>(dto));
        }
    }
}
