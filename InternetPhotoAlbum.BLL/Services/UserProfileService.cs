﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.DAL.Entities;
using InternetPhotoAlbum.DAL.UnitsOfWork.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.BLL.Services
{
    public class UserProfileService : IUserProfileService
    {
        private readonly IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> _uow;
        private readonly IMapper _mapper;

        public UserProfileService(IUnitOfWork<PhotoAlbumUser, PhotoAlbumRole> uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }

        /// <summary>
        /// Uses id of the <see cref="PhotoAlbumUser"/> to search
        /// for the corresponding <see cref="UserProfileDTO"/>
        /// </summary>
        /// <param name="userId">Id of the connected<see cref="PhotoAlbumUser"/></param>
        /// <returns>Found <see cref="UserProfileDTO"/> or null</returns>
        public async Task<UserProfileDTO> GetUserProfileByUserIdAsync(string userId)
        {
            return _mapper.Map<UserProfileDTO>(await _uow.UserProfileRepository.GetUserProfileByUserIdAsync(userId));
        }


        /// <summary>
        ///     Removes <see cref="UserProfile"/>
        ///     and connected <see cref="PhotoAlbumUser"/> from Database
        /// </summary>
        /// <param name="userId">Id of the connected <see cref="PhotoAlbumUser"/></param>
        public async Task RemoveUserProfileByUserIdAsync(string userId)
        {
            var profileToDelete = await _uow.UserProfileRepository.GetUserProfileByUserIdAsync(userId);
            var userToDelete = await _uow.PhotoAlbumUserManager.FindByIdAsync(userId);
            await _uow.UserProfileRepository.RemoveByIdAsync(profileToDelete.Id);
            await _uow.PhotoAlbumUserManager.DeleteAsync(userToDelete);
            await _uow.SaveAsync();
        }


        /// <summary>
        ///     Updates Username and Email of the <see cref="UserProfileDTO"/>
        ///     <para>
        ///         Updates Username and Email of the connected <see cref="PhotoAlbumUser"/>
        ///     </para>
        /// </summary>
        /// <param name="userProfile"><see cref="UserProfileDTO"/> object</param>
        /// <returns>Updated <see cref="UserProfileDTO"/></returns>
        public async Task<UserProfileDTO> UpdateUserProfileAsync(UserProfileDTO userProfile)
        {
            var userToEdit = await _uow.PhotoAlbumUserManager.FindByIdAsync(userProfile.PhotoAlbumUserId);
            var profileToEdit = await _uow.UserProfileRepository.GetByIdAsync(userToEdit.UserProfile.Id);
            if(userProfile.Username != null)
            {
                userToEdit.UserName = userProfile.Username;
                profileToEdit.Username = userProfile.Username;
            }
            if (userProfile.Email != null)
            {
                userToEdit.Email = userProfile.Email;
                profileToEdit.Email = userProfile.Email;
            }
            await _uow.SaveAsync();
            return _mapper.Map<UserProfileDTO>(profileToEdit);
        }
    }
}
