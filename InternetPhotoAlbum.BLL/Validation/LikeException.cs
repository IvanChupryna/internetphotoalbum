﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.Validation
{
    public class LikeException : Exception
    {
        public LikeException() : base() { }
        public LikeException(string message) : base(message) { }
    }
}
