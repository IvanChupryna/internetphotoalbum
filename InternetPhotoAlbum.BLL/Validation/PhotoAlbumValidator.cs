﻿using InternetPhotoAlbum.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.Validation
{
    internal static class PhotoAlbumValidator
    {
        internal static void ValidatePost(PostDTO post)
        {
            if(post.Image == null)
            {
                throw new PostException("You haven't provided an image");
            }
        }
    }
}
