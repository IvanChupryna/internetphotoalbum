﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.Validation
{
    public class PostException : Exception
    {
        public PostException() : base() { }
        public PostException(string message) : base(message) { }
    }
}
