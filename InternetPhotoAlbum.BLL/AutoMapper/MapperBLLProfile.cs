﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternetPhotoAlbum.BLL.AutoMapper
{
    public class MapperBLLProfile : Profile
    {
        public MapperBLLProfile()
        {
            CreateMap<Post, PostDTO>()
                .ForMember(dest => dest.LikesIds, opt => opt.MapFrom(src => src.Likes.Select(l => l.Id)))
                .ForMember(dest => dest.CommentsIds, opt => opt.MapFrom(src => src.Comments.Select(c => c.Id)))
                .ReverseMap();

            CreateMap<Like, LikeDTO>()
                .ReverseMap();

            CreateMap<UserProfile, UserProfileDTO>()
                .ReverseMap();

            CreateMap<PhotoAlbumUser, UserDTO>()
                .ForMember(dest => dest.UserProfileId, opt => opt.MapFrom(src => src.UserProfile.Id))
                .ReverseMap();

            CreateMap<RoleDTO, PhotoAlbumRole>()
                .ReverseMap();
        }
    }
}
