﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.DTOs
{
    public class RegisterDTO
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
