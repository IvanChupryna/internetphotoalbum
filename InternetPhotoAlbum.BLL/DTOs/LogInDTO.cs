﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.DTOs
{
    public class LogInDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
