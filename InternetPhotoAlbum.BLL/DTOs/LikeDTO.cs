﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.DTOs
{
    public class LikeDTO
    {
        public string UserId { get; set; }
        public int PostId { get; set; }
    }
}
