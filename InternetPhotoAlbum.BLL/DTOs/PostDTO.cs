﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InternetPhotoAlbum.BLL.DTOs
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string UserId { get; set; }
        public string PostText { get; set; }
        public DateTime PostedDate { get; set; }
        public ICollection<int> LikesIds { get; set; }
        public ICollection<int> CommentsIds { get; set; }
    }
}
