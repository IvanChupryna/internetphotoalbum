﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Models
{
    public class PostDisplayModel
    {
        public string Image { get; set; }
        public string PostText { get; set; }
        public DateTime PostedDate { get; set; }
        public int LikesCount { get; set; }
    }
}
