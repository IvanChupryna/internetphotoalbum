﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "You haven't entered Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You haven't entered a Password")]
        public string Password { get; set; }
    }
}
