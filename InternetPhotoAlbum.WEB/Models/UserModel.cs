﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        [MinLength(3, ErrorMessage = "New Username is too short")]
        public string Username { get; set; }
        [RegularExpression(@"^[a-zA-Z]+@[a-zA-Z]+\.[a-z]{3}", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
        public int UserProfileId { get; set; } 
    }
}
