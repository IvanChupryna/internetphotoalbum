﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Models
{
    public class PostEditModel
    {
        public string UserId { get; set; }

        [StringLength(1000)]
        public string PostText { get; set; }
    }
}
