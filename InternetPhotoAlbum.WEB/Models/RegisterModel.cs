﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "You haven't entered Username")]
        [MinLength(3, ErrorMessage = "Your username is too short")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You haven't entered an Email")]
        [RegularExpression(@"^[a-zA-Z]+[0-9]*@[a-zA-Z]+\.[a-z]{3}", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "You haven't entered a Password")]
        [MinLength(10, ErrorMessage = "Password is too short")]
        public string Password { get; set; }

        [Required(ErrorMessage = "You haven't confirmed the Password")]
        public string ConfirmPassword { get; set; }
    }
}
