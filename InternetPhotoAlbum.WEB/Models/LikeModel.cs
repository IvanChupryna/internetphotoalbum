﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Models
{
    public class LikeModel
    {
        public int UserProfileId { get; set; }
        public int PostId { get; set; }
    }
}
