﻿using InternetPhotoAlbum.BLL.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Filter
{
    public class ErrorHandlingFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            HandleExceptionAsync(context);
            context.ExceptionHandled = true;
        }

        private static void HandleExceptionAsync(ExceptionContext context)
        {
            var exception = context.Exception;

            switch (exception)
            {
                case PostException _:
                    SetExceptionResult(context, exception, HttpStatusCode.BadRequest);
                    break;
                case LikeException _:
                    SetExceptionResult(context, exception, HttpStatusCode.Conflict);
                    break;
                case UserException _:
                    SetExceptionResult(context, exception, HttpStatusCode.NotFound);
                    break;
                default:
                    SetExceptionResult(context, exception, HttpStatusCode.BadRequest);
                    break;
            }
        }

        private static void SetExceptionResult(
            ExceptionContext context,
            Exception exception,
            HttpStatusCode code)
        {
            context.Result = new JsonResult(new
            {
                StatusCode = (int)code,
                Message = exception.Message
            });
        }
    }
}
