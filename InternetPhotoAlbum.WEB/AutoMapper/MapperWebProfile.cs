﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.WEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.AutoMapper
{
    public class MapperWebProfile : Profile
    {
        public MapperWebProfile()
        {
            CreateMap<RegisterModel, RegisterDTO>();
            CreateMap<LoginModel, LogInDTO>();
            CreateMap<PostDTO, PostDisplayModel>()
                .ForMember(dest => dest.LikesCount, src => src.MapFrom(dto => dto.LikesIds.Count));
            CreateMap<PostDTO, PostEditModel>();
            CreateMap<LikeDTO, LikeModel>();
            CreateMap<RoleDTO, RoleModel>()
                .ReverseMap();
            CreateMap<UserProfileDTO, UserProfileModel>()
                .ReverseMap();
            CreateMap<UserDTO, UserModel>()
                .ReverseMap();
        }
    }
}
