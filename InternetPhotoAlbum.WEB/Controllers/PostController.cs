﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.BLL.Validation;
using InternetPhotoAlbum.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InternetPhotoAlbum.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostController : ControllerBase
    {

        private readonly IPostService _postService;
        private readonly ILikeService _likeService;
        private readonly IMapper _mapper;

        public PostController(IPostService postService, ILikeService likeService, IMapper mapper)
        {
            _postService = postService;
            _likeService = likeService;
            _mapper = mapper;
        }
        // GET: api/<PostController>
        [HttpGet]
        public ActionResult<IEnumerable<PostDisplayModel>> GetAllPosts()
        {
            return Ok(_mapper.Map<IEnumerable<PostDisplayModel>>(_postService.GetAll()));
        }

        [HttpGet("date")]
        public ActionResult<IEnumerable<PostDisplayModel>> GetPostsSortedByDate()
        {
            return Ok(_mapper.Map<IEnumerable<PostDisplayModel>>(_postService.GetSortedByDate()));
        }

        [HttpGet("{date}")]
        public ActionResult<IEnumerable<PostDisplayModel>> GetPostsByDate(string date)
        {
                DateTime dateTime = Convert.ToDateTime(date);
                return Ok(_mapper.Map<PostDisplayModel>(_postService.GetByDate(dateTime)));
        }

        [HttpGet("likes")]
        public ActionResult<IEnumerable<PostDisplayModel>> GetPostsSortedByLikes()
        {
            return Ok(_mapper.Map<IEnumerable<PostDisplayModel>>(_postService.GetSortedByLikes()));
        }

        [HttpGet("user/{userName}")]
        public ActionResult<IEnumerable<PostDisplayModel>> GetPostsByUsername(string userName)
        {
                return Ok(_mapper.Map<IEnumerable<PostDisplayModel>>(_postService.GetPostsByUsername(userName)));
        }

        // GET api/<PostController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PostDisplayModel>> GetPostByIdAsync(int id)
        {
            try
            {
                return Ok(_mapper.Map<PostDisplayModel>(await _postService.GetByIdAsync(id)));
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/likes")]
        public async Task<ActionResult<IEnumerable<LikeModel>>> GetAllLikesOfCurrentPost(int id)
        {
                return Ok(_mapper.Map<IEnumerable<LikeModel>>(await _likeService.GetAllLikesOfCurrentPostAsync(id)));
        }

        // POST api/<PostController>
        [HttpPost]
        public async Task<ActionResult> AddPostAsync([FromForm] PostUploadModel post)
        {
                byte[] imageData = null;
                using (var binaryReader = new BinaryReader(post.ImageFile.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)post.ImageFile.Length);
                }

                var postDTO = new PostDTO()
                {
                    UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                    PostText = post.PostText,
                    PostedDate = DateTime.Now,
                    Image = Convert.ToBase64String(imageData)
                };

                await _postService.AddAsync(postDTO);
                return Created("api/[controller]", post);
        }

        [HttpPost("{postId}/like")]
        public async Task<ActionResult> AddLikeToPostAsync(int postId)
        {
            await _likeService.AddLikeToPostAsync(User.FindFirst(ClaimTypes.NameIdentifier).Value, postId);
            return Created("api/[controller]/{postId}/like/{userId}", postId);
        }
        // PUT api/<PostController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> EditPostAsync(int id, [FromForm] PostEditModel post)
        {
            var postInitial = _mapper.Map<PostEditModel>(await _postService.GetByIdAsync(id));
            if (postInitial == null)
            {
                return NotFound("There's not post with such Id");
            }
            if (postInitial.UserId != User.FindFirst(ClaimTypes.NameIdentifier).Value &&
               User.FindFirst(ClaimTypes.Role).Value != "Admin")
            {
                return BadRequest("You don't have permision to edit post of another user");
            }
            post.UserId = postInitial.UserId;
            await _postService.UpdateAsync(_mapper.Map<PostDTO>(post));
            return Ok();
        }

        // DELETE api/<PostController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var postToDelete = await _postService.GetByIdAsync(id);
            if (postToDelete.UserId != User.FindFirst(ClaimTypes.NameIdentifier).Value &&
                User.FindFirst(ClaimTypes.Role).Value != "Admin")
            {
                return BadRequest("You don't have permision to delete post of another user");
            }

            await _postService.RemoveByIdAsync(id);
            return NoContent();
        }

        [HttpDelete("{postId}/like")]
        public async Task<ActionResult> RemoveLikeFromPostAsync(int postId)
        {
                await _likeService.RemoveLikeFromPostAsync(User.FindFirst(ClaimTypes.NameIdentifier).Value, postId);
                return NoContent();
        }
    }
}
