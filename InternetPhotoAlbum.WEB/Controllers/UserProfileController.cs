﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserProfileController : ControllerBase
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IPostService _postService;
        private readonly IMapper _mapper;

        public UserProfileController(IUserProfileService userProfileService, IPostService postService, IMapper mapper)
        {
            _userProfileService = userProfileService;
            _postService = postService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<UserProfileModel>> GetUserProfileAsync()
        {
            var userProfile = _mapper.Map<UserProfileModel>(await _userProfileService.GetUserProfileByUserIdAsync(
                User.FindFirst(ClaimTypes.NameIdentifier).Value));

            return Ok(userProfile);
        }

        [HttpGet("posts")]
        public ActionResult<IEnumerable<PostDisplayModel>> GetUserPosts()
        {
            var userPosts = _postService.GetPostsByUsername(User.FindFirst(ClaimTypes.Name).Value);
            return Ok(_mapper.Map<IEnumerable<PostDisplayModel>>(userPosts));
        }

        [HttpPut]
        public async Task<ActionResult<UserProfileModel>> EditUserProfileAsync([FromForm]UserProfileModel userProfile)
        {
            if(userProfile == null)
            {
                return BadRequest("Passed User Profile is null");
            }

            userProfile.PhotoAlbumUserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            userProfile.Id = (await _userProfileService.GetUserProfileByUserIdAsync(userProfile.PhotoAlbumUserId)).Id;
            if (ModelState.IsValid)
            {
                await _userProfileService.UpdateUserProfileAsync(_mapper.Map<UserProfileDTO>(userProfile));
                return Ok(userProfile);
            }
            return BadRequest(ModelState.Values);
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteUserProfileAsync()
        {
            await _userProfileService.RemoveUserProfileByUserIdAsync(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            return NoContent();
        }
    }
}
