﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.BLL.Services.Utilities;
using InternetPhotoAlbum.WEB.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizeController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AuthorizeController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserManagerResponse>> RegisterAsync([FromBody] RegisterModel registerModel)
        {
            var hasNumbers = new Regex(@"[0-9]+");
            var hasUpper = new Regex(@"[A-Z]+");
            var hasLower = new Regex(@"[a-z]+");
            string password = registerModel.Password;

            if(!hasNumbers.IsMatch(password) || !hasUpper.IsMatch(password) || !hasLower.IsMatch(password))
            {
                ModelState.AddModelError("Password", "Invalid Password");
            }

            if (ModelState.IsValid)
            {
               var result = await _userService.RegisterUserAsync(_mapper.Map<RegisterDTO>(registerModel));
                if (result.IsSucceeded)
                {
                    return Ok(result.Message);
                }

                return BadRequest(result.Errors);
            }
            return BadRequest(ModelState.Values);
        }

        [HttpPost("login")]
        public async Task<ActionResult<UserManagerResponse>> LoginAsync([FromBody] LoginModel login)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.LoginAsync(_mapper.Map<LogInDTO>(login));
                if (result.IsSucceeded)
                {
                    return Ok(result);
                }
                return BadRequest(result.Message);
            }
            return Ok(ModelState.Values);
        }
    }
}
