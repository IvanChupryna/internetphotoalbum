﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.BLL.Services.Utilities;
using InternetPhotoAlbum.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace InternetPhotoAlbum.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [Authorize(Roles = "Admin, User")]
        [HttpGet]
        public ActionResult<IEnumerable<UserModel>> GetAllUsers()
        {
            return Ok(_mapper.Map<IEnumerable<UserModel>>(_userService.GetAllUsers()));
        }

        [Authorize(Roles = "Admin, User")]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserModel>> GetUserByIdAsync(string id)
        {
            return Ok(_mapper.Map<UserModel>(await _userService.GetUserByIdAsync(id)));
        }

        [Authorize(Roles = "Admin, User")]
        [HttpGet("name/{username}")]
        public async Task<ActionResult<UserModel>> GetUserByNameAsync(string username)
        {
            return Ok(_mapper.Map<UserModel>(await _userService.GetUserByNameAsync(username)));
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<ActionResult<UserManagerResponse>> EditUser(UserModel userModel)
        {
            if(userModel == null)
            {
                return BadRequest("Passed User is null");
            }

            userModel.Id = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            if(ModelState.IsValid)
            {
                var result = await _userService.EditUserAsync(_mapper.Map<UserDTO>(userModel));
                return Ok(result.Message);
            }
            return BadRequest(ModelState.Values);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete]
        public async Task<ActionResult> DeleteUserById(string id)
        {
            await _userService.DeleteUserByIdAsync(id);
            return NoContent();
        }
    }
}
