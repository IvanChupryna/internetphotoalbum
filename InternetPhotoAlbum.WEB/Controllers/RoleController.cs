﻿using AutoMapper;
using InternetPhotoAlbum.BLL.DTOs;
using InternetPhotoAlbum.BLL.Services.Abstract;
using InternetPhotoAlbum.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InternetPhotoAlbum.WEB.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;

        public RoleController(IRoleService roleService, IMapper mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }
        // GET: api/<RoleController>
        [HttpGet("{name}")]
        public async Task<ActionResult<RoleModel>> GetRoleByNameAsync(string name)
        {
            var role = _mapper.Map<RoleModel>(await _roleService.GetRoleByName(name));
            return Ok(role);
        }

        // POST api/<RoleController>
        [HttpPost]
        public async Task<ActionResult> AddRoleAsync([FromBody] RoleModel role)
        {
            await _roleService.AddAsync(_mapper.Map<RoleDTO>(role));
            return Created("api/[controller]", role);
        }

        // PUT api/<RoleController>/5
        [HttpPut("{name}")]
        public async Task<ActionResult> UpdateRoleAsync(string name, [FromBody] RoleModel role)
        {
            var roleToUpdate = _mapper.Map<RoleModel>(await _roleService.GetRoleByName(name));
            roleToUpdate.Name = role.Name;
            await _roleService.UpdateAsync(_mapper.Map<RoleDTO>(roleToUpdate));
            return Ok();
        }

        // DELETE api/<RoleController>/5
        [HttpDelete("{name}")]
        public async Task<ActionResult> Delete(string name)
        {
            await _roleService.RemoveByNameAsync(name);
            return NoContent();
        }
    }
}
